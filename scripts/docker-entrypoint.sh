#!/usr/bin/env bash

export DELUGE_NAME="${DELUGE_NAME:-deluge}"
export DELUGE_UID="${DELUGE_UID:-1000}"
export DELUGE_GID="${DELUGE_GID:-1000}"

export DELUGE_LOG_LEVEL="${DELUGE_LOG_LEVEL:-info}"
export DELUGE_CONFIG_DIR="${DELUGE_CONFIG_DIR:-"/deluge/config"}"

case $CONTAINER_TYPE in
  daemon)
    EXEC_SCRIPT=/deluge/scripts/deluge-daemon.sh
    ;;
  webui)
    EXEC_SCRIPT=/deluge/scripts/deluge-web.sh
    ;;
  *)
    echo "Set CONTAINER_TYPE to one of 'webui' or 'daemon'"
    exit 1
    ;;
esac

/deluge/scripts/deluge-user.sh

exec "$EXEC_SCRIPT"
