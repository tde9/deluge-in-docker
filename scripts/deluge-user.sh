#!/usr/bin/env bash

set -euo pipefail

function create_user() {
  echo "Create group $DELUGE_NAME with id $DELUGE_GID"
  addgroup --gid "$DELUGE_GID" "$DELUGE_NAME"

  echo "Create user $DELUGE_NAME with id $DELUGE_UID"
  adduser \
    --gecos "" \
    --no-create-home \
    --uid "$DELUGE_UID" \
    --ingroup "$DELUGE_NAME" \
    --disabled-login \
    --disabled-password \
    "$DELUGE_NAME"
}

function fix_config_dir_permissions() {
  echo "Ensuring directory $DELUGE_CONFIG_DIR exists and is owned by $DELUGE_NAME:$DELUGE_NAME"

  if ! [ -d "$DELUGE_CONFIG_DIR" ]; then
    echo "Creating new configuration directory $DELUGE_CONFIG_DIR."
    echo "Deluge will populate it with defaults on start. You should update it afterwards!"
    mkdir -p "$DELUGE_CONFIG_DIR"
  else
    echo "Directory $DELUGE_CONFIG_DIR already exists. Not creating."
  fi

  echo "Ensure owner of $DELUGE_CONFIG_DIR is $DELUGE_NAME:$DELUGE_NAME"
  chown -R "$DELUGE_NAME:$DELUGE_NAME" "$DELUGE_CONFIG_DIR"
}

create_user
fix_config_dir_permissions
