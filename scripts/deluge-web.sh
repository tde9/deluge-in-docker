#!/usr/bin/env bash

set -euo pipefail

export DELUGE_WEB_HOST="${DELUGE_WEB_HOST:-0.0.0.0}"
export DELUGE_WEB_PORT="${DELUGE_WEB_PORT:-8112}"

set -x
exec deluge-web \
  --interface "$DELUGE_WEB_HOST" \
  --port "$DELUGE_WEB_PORT" \
  --loglevel "$DELUGE_LOG_LEVEL" \
  --do-not-daemonize \
  --user "$DELUGE_NAME" \
  --group "$DELUGE_NAME" \
  --config "$DELUGE_CONFIG_DIR"
