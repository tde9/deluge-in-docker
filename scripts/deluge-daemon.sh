#!/usr/bin/env bash

set -euo pipefail

export DELUGED_HOST="${DELUGED_HOST:-0.0.0.0}"
export DELUGED_PORT="${DELUGED_PORT:-58846}"

set -x
exec deluged \
  --interface "$DELUGED_HOST" \
  --port "$DELUGED_PORT" \
  --loglevel "$DELUGE_LOG_LEVEL" \
  --do-not-daemonize \
  --user "$DELUGE_NAME" \
  --group "$DELUGE_NAME" \
  --config "$DELUGE_CONFIG_DIR"
