FROM python:3.7-slim as base

ENV PYTHONFAULTHANDLER=1 \
    PYTHONUNBUFFERED=1 \
    PYTHONHASHSEED=random \
    DELUGE_ROOTDIR=/deluge

FROM base as build

ENV PIP_NO_CACHE_DIR=on \
    PIP_DISABLE_PIP_VERSION_CHECK=on \
    PIP_DEFAULT_TIMEOUT=100 \
    PIPENV_VENV_IN_PROJECT=1

WORKDIR /deluge
COPY Pipfile      /deluge/Pipfile
COPY Pipfile.lock /deluge/Pipfile.lock

# Dev dependencies
RUN apt update && apt install -qq -y --no-install-recommends \
      build-essential \
      libgeoip-dev \
      libfreetype-dev \
      libfribidi-dev \
      libharfbuzz-dev \
      libjpeg-dev \
      liblcms2-dev \
      libtorrent-dev \
      libtiff-dev \
      tcl-dev \
      zlib1g-dev

RUN pip install --upgrade pip setuptools wheel && \
    pip install pipenv && \
    cat Pipfile && \
    pipenv --python 3.7 && \
    pipenv install && \
    pip uninstall --yes pipenv

FROM base as dist

COPY --from=build /deluge /deluge

RUN apt -qq update && apt install -qq -y --no-install-recommends \
      bash \
      gettext \
      libfreetype6 \
      libfribidi-bin \
      libgeoip1 \
      libharfbuzz-bin \
      libjpeg-tools \
      liblcms2-2 \
      libtorrent21 \
      libtiff5 \
      vim && \
    rm -rf /var/lib/apt/lists

ENV PATH "/deluge/.venv/bin:$PATH"
WORKDIR /deluge
COPY scripts /deluge/scripts

CMD /deluge/scripts/docker-entrypoint.sh
